import { pizzaSelectUser } from "./index.js";
import { show } from "./functions.js";
import { userSlectTopping } from "./functions.js";
import {
  clickInputSize,
  clickSauceAdd,
  clickToppingAdd,
} from "./functionEvent.js";
import pizza from "./pizza.js";

function dragAndDrop() {
  const dragPizza = document.querySelector(".table-wrapper .table");
  function onDragStart(event) {
    const check = event.target.getAttribute("data-id");

    if (check === "sauce") {
      const sauceAlt = event.target.alt;
      if (!pizzaSelectUser.sauceName.includes(sauceAlt)) {
        const sauceId = event.target.id;
        pizzaSelectUser.sauceName.push(sauceAlt);
        const targetSauce = pizza.sauce.find((el) => el.name === sauceId);
        pizzaSelectUser.sauce.push(targetSauce);
        show(pizzaSelectUser);
      }
    } else if (check === "topping") {
      const toppingAlt = event.target.alt;
      if (!pizzaSelectUser.toppingName.includes(toppingAlt)) {
        const toppingId = event.target.id;
        pizzaSelectUser.toppingName.push(toppingAlt);
        const targetToping = pizza.topping.find((el) => el.name === toppingId);
        pizzaSelectUser.topping.push(targetToping);
        show(pizzaSelectUser);
      }
    }
    event.dataTransfer.effectAllowed = "copy";
    event.dataTransfer.setData("text/plain", this.src);
  }
  function dragEnd(event) {
    event.preventDefault();
    event.stopPropagation();
  }
  function onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
  }
  function onDrop(event) {
    const imageData = event.dataTransfer.getData("text/plain");
    const newImage = new Image();
    newImage.src = imageData;
    this.appendChild(newImage);

    showInfo();
  }
  dragPizza.addEventListener("dragover", onDragOver);
  dragPizza.addEventListener("drop", onDrop);
  //
  const ingridients = document.querySelectorAll(".ingridients .draggable");
  ingridients.forEach((ingridient) => {
    ingridient.addEventListener("dragstart", onDragStart);
    ingridient.addEventListener("dragend", dragEnd);
  });
}
dragAndDrop();

function showInfo() {
  const saucesElem = document.querySelector(".sauces > div > div");
  saucesElem.textContent = `${pizzaSelectUser.sauceName.join("\n")}`;
  document.querySelector(
    ".topings > div > div"
  ).textContent = `${pizzaSelectUser.toppingName.join("\n")}`;

  userSlectTopping();
}
export { dragAndDrop, showInfo };
